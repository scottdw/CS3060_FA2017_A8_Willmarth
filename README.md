In order to run problem 1, use ends_in_3(num)
3 -> true
6 -> false
133343 -> true

In order to run problem 2, use is_prime(num)
5 -> true
9 -> false
113 -> true

In order to run problem 3, use find_prime3(num)
find_prime3(5) ->
3	
13	
23	
43	
53

In order to run problem 4, use sorting({list})
sorting({6,5,88,96,43,22,1}) ->
Sorted	
1	
5	
6	
22	
43	
88	
96	

Reverse sort	
96	
88	
43	
22	
6	
5	
1	

Odd index only	
96	
43	
6	
1

In order to run problem 5, use SelectionSort()
1	
1	
1	
2	
4	
4	
7	
11	
16	
16	
16	
19	
19	
19	
20	
20	
21	
23	
23	
24	
24	
26	
27	
27	
30	
31	
31	
31	
32	
32	
32	
35	
35	
36	
37	
39	
39	
39	
40	
41	
41	
44	
49	
50	
51	
52	
54	
54	
55	
56	
56	
56	
56	
57	
60	
62	
62	
62	
63	
65	
65	
65	
66	
66	
66	
67	
68	
68	
68	
69	
69	
70	
71	
71	
72	
72	
74	
74	
78	
79	
80	
81	
81	
82	
82	
83	
83	
84	
85	
86	
87	
88	
88	
89	
91	
92	
93	
94	
94	
94

In order to run problem 6, use reduce(max, init, f). You will have to go into the code to define function f
print(reduce(9, 0, f))
f =
function(max, init)
    for i = 1, max do
      init = init + i
    end
    return init
end

-> 45

In order to run problem 7, use reduce(max, init, f)
print(reduce(5, 0, f)) -> 120