function order_it(t, order)
  local list = {}
  for k in pairs(t) do list[#list+1] = k end
  if order then
      table.sort(list, function(a,b) return order(t, a, b) end)
  else
      table.sort(list)
  end

  local i = 0
  return function()
      i = i + 1
      if list[i] then
          return list[i], t[list[i]]
      end
  end
end

function sorting(list)
  print("Sorted")
  for k,v in order_it(list, function(t,a,b) return t[b] > t[a] end) do
    print(v)
  end
  print()
  print("Reverse sort")
  revlist = {}
  for k,v in order_it(list, function(t,a,b) return t[b] < t[a] end) do
    print(v)
    table.insert(revlist, v)
  end
  print()
  print("Odd index only")
  for i = 1, table.getn(revlist) do
    if(i % 2 == 1) then
      print(revlist[i])
    end
  end

end
