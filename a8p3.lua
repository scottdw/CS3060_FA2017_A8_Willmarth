function has_value (tab, val)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end

    return false
end

function ends_in_3(num)
  if(num == 3) then
    return true
  else
    while(num ~= num % 10) do
      if(math.fmod(num, 10) == 3) then
       return true
      else
        num = num % 10
      end
    end
  end
   return false
end

function is_prime(num)
  for i=2, num do
    if(i ~= num and math.fmod(num, i) == 0) then
      return false
    end
  end
  return true
end


function find_prime3(n)
  counter = 3
  i = 0
  while i < n do

    if(ends_in_3(counter)) then
      if(is_prime(counter)) then
        print(counter)
        i = i + 1
      end
    end
    counter = counter + 1
  end
  return 0
end
