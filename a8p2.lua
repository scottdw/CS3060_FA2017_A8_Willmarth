function is_prime(num)
  for i=2, num do
    if(i ~= num and math.fmod(num, i) == 0) then
      return false
    end
  end
  return true
end
