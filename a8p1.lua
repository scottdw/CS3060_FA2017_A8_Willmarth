function ends_in_3(num)
  if(num == 3) then
    return true
  else
    while(num ~= num % 10) do
      if(math.fmod(num, 10) == 3) then
       return true
      else
        num = num % 10
      end
    end
  end
   return false
end
