function reduce(max, init, f)
  init = f(max, init)
  return init
end

f =
function(max, init)
  init = 1
    for i = 1, max do
      init = init * i
    end
    return init
end
